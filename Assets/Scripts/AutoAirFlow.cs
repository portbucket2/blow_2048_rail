using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAirFlow : MonoBehaviour
{
    public float base_force;
    private List<Rigidbody> bodies = new List<Rigidbody>();


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            bodies.Add(other.GetComponent<Rigidbody>());
            other.GetComponent<BallBehavior>().enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            bodies.Remove(other.GetComponent<Rigidbody>());
        }
    }

    private void FixedUpdate()
    {
        foreach (Rigidbody rb in bodies)
        {
            Vector3 pos_diff = (rb.transform.position - transform.position);

            rb.AddForce(pos_diff.normalized * base_force, ForceMode.Force);
        }
    }
}
