using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RotationDirection
{
    Clockwise,
    CounterClockwise
}

public class DragRotator : MonoBehaviour
{
    public float sensitivity;

    public List<RotatableObject> rotatable_objects;

    private Vector3 last_touch_pos = Vector3.zero;


    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 touch_relative = last_touch_pos - Input.mousePosition;

            foreach (RotatableObject item in rotatable_objects)
            {
                item.target_object.transform.Rotate(Vector3.forward, touch_relative.x * sensitivity);
            }

            last_touch_pos = Input.mousePosition;
        }
    }
}


[System.Serializable]
public class RotatableObject
{
    public GameObject target_object;
    //public RotationDirection rotation_direction;
    public int direction = 1;
}
