using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisableDelayed : MonoBehaviour
{
    public float enable_delay;
    public float disable_delay;

    public List<GameObject> objects_to_enable;
    public List<GameObject> objects_to_disable;

    public void EnableObjects()
    {
        Invoke("EnableObjectsDelayed", enable_delay);
    }

    private void EnableObjectsDelayed()
    {
        foreach (GameObject item in objects_to_enable)
        {
            item.SetActive(true);
        }
    }


    public void DisableObjects()
    {
        Invoke("DisableObjectsDelayed", disable_delay);
    }

    private void DisableObjectsDelayed()
    {
        foreach (GameObject item in objects_to_disable)
        {
            item.SetActive(false);
        }
    }

}
