using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public static class Stats
{
    public static int attemptNum = 1;

    public static bool resetLevels = false;
    public static bool testing = false;
    public static int levelToTest = 1;

    public static int currentFakeLevel = 1;

    public static int CalculateNextLevel(int step)
    {
        currentFakeLevel += step;

        if (testing)
        {
            if (currentFakeLevel >= SceneManager.sceneCountInBuildSettings)
            {
                int levelToLoad = ((levelToTest) % (SceneManager.sceneCountInBuildSettings - 1));

                if (levelToLoad == 0)
                {
                    levelToLoad = SceneManager.sceneCountInBuildSettings - 1;
                }

                return levelToLoad;
            }
            else
            {
                return currentFakeLevel;
            }
        }
        else
        {
            if (currentFakeLevel >= SceneManager.sceneCountInBuildSettings)
            {
                int levelToLoad = ((currentFakeLevel) % (SceneManager.sceneCountInBuildSettings - 1));

                if (levelToLoad == 0)
                {
                    levelToLoad = SceneManager.sceneCountInBuildSettings - 1;
                }

                Debug.Log("Actual Level " + levelToLoad.ToString());

                return levelToLoad;
            }
            else
            {
                return currentFakeLevel;
            }
        }
    }
}
