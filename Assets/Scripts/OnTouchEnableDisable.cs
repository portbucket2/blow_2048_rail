using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTouchEnableDisable : MonoBehaviour
{
    public List<GameObject> enable_on_touch;
    public List<GameObject> disable_on_touch;

    private void OnMouseDown()
    {
        foreach (GameObject item in enable_on_touch)
        {
            item.SetActive(true);
        }

        foreach (GameObject item in disable_on_touch)
        {
            item.SetActive(false);
        }
    }

    private void OnMouseUp()
    {
        foreach (GameObject item in enable_on_touch)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in disable_on_touch)
        {
            item.SetActive(true);
        }
    }
}
