using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;

public class LevelManager : MonoBehaviour
{
    public int next_level;
    public GameObject LevelEndUI;

    public float fan_toggle;
    public Slider slider;

    public bool lock_broken = false;

    public bool setup_and_observe = false;

    private void Awake()
    {
        Physics.gravity = Vector3.zero;

        PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
    }

    private void Start()
    {
        LionAnalytics.LevelStart(Stats.currentFakeLevel, Stats.attemptNum);

        Debug.LogWarning("Starting -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());
    }

    private void Update()
    {
        if (!setup_and_observe)
        {
            fan_toggle = slider.value;
        }
    }

    public void LevelEnd(float delay)
    {
        Invoke("LevelEndDelayed", delay);
        lock_broken = true;

        LionAnalytics.LevelComplete(Stats.currentFakeLevel, Stats.attemptNum);

        Debug.LogWarning("Complete -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());
    }

    private void LevelEndDelayed()
    {
        LevelEndUI.SetActive(true);
    }

    public void NextLevel()
    {
        Stats.attemptNum = 1;

        SceneManager.LoadScene(Stats.CalculateNextLevel(1));
    }

    public void Restart()
    {
        LionAnalytics.LevelRestart(Stats.currentFakeLevel, Stats.attemptNum);
     
        Stats.attemptNum++;

        Debug.LogWarning("Restart -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void FanStart()
    {
        fan_toggle = 1;
    }
}
