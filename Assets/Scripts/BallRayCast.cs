using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRayCast : MonoBehaviour
{
    private FanContainer fan_container;
    private RaycastHit raycast;

    public List<bool> fan_ray_hit = new List<bool>();

    private void Awake()
    {
        fan_container = FindObjectOfType<FanContainer>();

        for (int i = 0; i < fan_container.fan_roots.Count; i++)
        {
            fan_ray_hit.Add(false);
        }
    }

    private void FixedUpdate()
    {
        // Bit shift the index of the layer (7) to get a bit mask
        int layerMask = 1 << 7;

        // This would cast rays only against colliders in layer 7.
        // But instead we want to collide against everything except layer 7. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        for (int i = 0; i < fan_container.fan_roots.Count; i++)
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(fan_container.fan_roots[i].transform.position - transform.position), out raycast, 100, layerMask))
            {

                if (raycast.collider.gameObject.CompareTag("FanRayTarget"))
                {
                    fan_ray_hit[i] = true;
                    Debug.DrawRay(transform.position, transform.TransformDirection(fan_container.fan_roots[i].transform.position - transform.position), Color.green); 
                }
                else
                {
                    fan_ray_hit[i] = false;
                    Debug.DrawRay(transform.position, transform.TransformDirection(fan_container.fan_roots[i].transform.position - transform.position), Color.red);
                }

            }
            else
            {
                fan_ray_hit[i] = false;
                Debug.DrawRay(transform.position, transform.TransformDirection(fan_container.fan_roots[i].transform.position - transform.position), Color.red);
            }
        }

    }
}
