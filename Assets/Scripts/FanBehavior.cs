using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanBehavior : MonoBehaviour
{
    public int id = 0;

    public float base_force;
    public float falloff_range = 15f;
    //private List<Rigidbody> bodies = new List<Rigidbody>();
    private List<BallBehavior> balls = new List<BallBehavior>();

    [SerializeField]
    private int toggle = 0;

    private LevelManager levelManager;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            //bodies.Add(other.GetComponent<Rigidbody>());
            balls.Add(other.GetComponent<BallBehavior>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            //bodies.Remove(other.GetComponent<Rigidbody>());
            balls.Remove(other.GetComponent<BallBehavior>());
        }
    }

    private void FixedUpdate()
    {
        //foreach (Rigidbody rb in bodies)
        foreach (BallBehavior ball in balls)
        {
            Vector3 pos_diff = (ball.transform.position - transform.position);
            float falloff_multiplier = (falloff_range - pos_diff.magnitude) / falloff_range;

            //rb.AddForce(pos_diff.normalized * base_force * falloff_multiplier * toggle, ForceMode.Force);
            ball.GetPushed(id, pos_diff.normalized * base_force * falloff_multiplier * levelManager.fan_toggle);
        }
    }

    public void Toggle()
    {
        toggle = 1 - toggle;
    }
}
