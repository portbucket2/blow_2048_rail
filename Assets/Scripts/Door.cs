using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Door : MonoBehaviour
{
    public Animator animator;

    public int id_required;
    public TMP_Text number;

    public List<Material> id_wise_mats = new List<Material>();
    public MeshRenderer lock_mesh;
    
    public UnityEvent OnDoorOpen;

    private LevelManager levelManager;

    private void Awake()
    {
        number.text = Mathf.Pow(2, id_required).ToString();
        levelManager = FindObjectOfType<LevelManager>();

        int temp_id = (id_required % id_wise_mats.Count);
        lock_mesh.material = id_wise_mats[temp_id];
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            BallBehavior ball = collision.gameObject.GetComponent<BallBehavior>();

            if (ball.id < id_required)
            {
                animator.Play("door_almost_open", -1, 0);
            }
            else
            {
                animator.Play("door_open", -1, 0);

                GetComponent<BoxCollider>().enabled = false;

                OnDoorOpen?.Invoke();

                levelManager.LevelEnd(2f);

                Vibrator.Vibrate(100);
            }
        }
    }

}
