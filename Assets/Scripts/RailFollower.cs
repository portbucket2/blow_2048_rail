using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class RailFollower : MonoBehaviour
{
    public enum StartPosition
    {
        start,
        end
    }

    public StartPosition startPosition;

    public Vector3 offset;

    public PathCreator pathCreator;

    [Range(0f,1f)]
    public float interpolated_position;

    public float distance;
    private float distance_target;
    public float speed;

    private Vector3 touch_offset = Vector3.zero;
    private float touch_z_coord;

    Camera cam;
    private void Awake()
    {
        cam = Camera.main;

        distance = Mathf.Lerp(0, 0.9999f, interpolated_position) * pathCreator.path.length;
        distance_target = distance;
    }


    private Vector3 GetMouseWorldPos()
    {
        Vector3 touchpos = Input.mousePosition;

        touchpos.z = touch_z_coord;

        return cam.ScreenToWorldPoint(touchpos);
    }

    private void OnMouseDown()
    {
        touch_z_coord = cam.WorldToScreenPoint(transform.position).z;

        touch_offset = transform.position - GetMouseWorldPos();
    }

    private void OnMouseDrag()
    {
        Vector3 world_mouse_pos = GetMouseWorldPos() + touch_offset;

        Vector3 closest_point = pathCreator.path.GetClosestPointOnPath(world_mouse_pos);

        distance_target = pathCreator.path.GetClosestDistanceAlongPath(closest_point);
    }

    private void Update()
    {
        distance = Mathf.Lerp(distance, distance_target, Time.deltaTime * speed);
        UpdateLocRot();
    }


    private void UpdateLocRot()
    {
        transform.position = pathCreator.path.GetPointAtDistance(distance) + offset;
        transform.rotation = pathCreator.path.GetRotationAtDistance(distance);
    }
}
