using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class RailStatic : MonoBehaviour
{
    public PathCreator pathCreator;

    public float distance;

    private void Awake()
    {
        transform.position = pathCreator.path.GetPointAtDistance(distance);
    }
}
