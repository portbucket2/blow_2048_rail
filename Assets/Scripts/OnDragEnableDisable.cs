using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDragEnableDisable : MonoBehaviour
{
    public float delay;
    public bool execute_once;

    public List<GameObject> enable_on_drag;
    public List<GameObject> disable_on_drag;

    private void OnMouseDrag()
    {
        Invoke("DelayedExecution", delay);
    }

    private void DelayedExecution()
    {
        foreach (GameObject item in enable_on_drag)
        {
            item.SetActive(true);
        }

        foreach (GameObject item in disable_on_drag)
        {
            item.SetActive(false);
        }

        if (execute_once)
        {
            enabled = false;
        }
    }
}
