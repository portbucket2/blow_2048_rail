using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpecificEnableDisable : MonoBehaviour
{
    public int target_level;

    public List<GameObject> enable_on_level;
    public List<GameObject> disable_on_level;

    private void Awake()
    {
        if (Stats.currentFakeLevel == target_level)
        {
            foreach (GameObject item in enable_on_level)
            {
                item.SetActive(true);
            }

            foreach (GameObject item in disable_on_level)
            {
                item.SetActive(false);
            }
        }

    }
}
